# importing libraries
import json

import flask
from flask import Flask
from flask_mail import Mail, Message
from flask import Flask, request, jsonify

app = Flask(__name__)
mail = Mail(app)  # instantiate the mail class

# configuration of mail
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'virgildanciu90@gmail.com'
app.config['MAIL_PASSWORD'] = 'XXXXXXXXXX'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
mail = Mail(app)


# message object mapped to a particular URL ‘/’
@app.route("/", methods=['POST'])
def index():
    msg = Message(
        'Mesaj de la DentaWeb',
        sender='virgildanciu90@gmail.com',
        recipients=['virgildanciu90@gmail.com']
    )
    data = request.data
    #msg.body = f'Nume: {data.firstName} Prenume: {data.lastName}, Telefon: {data.phone} \n Mesaj: {data.message}'
    y = json.loads(data)
    firstName = y["firstName"]
    lastname = y["lastName"]
    message = y["message"]
    phone = y["phone"]

    msg.body = 'Nume: '+ firstName +'\n'+ 'Prenume: '+ lastname +'\n' +'Telefon: ' + phone + '\n' + 'Mesaj: '+ message
    mail.send(msg)
    response = flask.jsonify({'response': 'success'})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


if __name__ == '__main__':
    app.run(debug=True)