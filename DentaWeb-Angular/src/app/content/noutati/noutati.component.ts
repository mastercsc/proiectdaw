import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { switchMap } from "rxjs/operators";


declare const require;
const xml2js = require("xml2js");

@Component({
  selector: 'app-noutati',
  templateUrl: './noutati.component.html',
  styleUrls: ['./noutati.component.scss']
})
export class NoutatiComponent implements OnInit {
  news$: Observable<Array<any>>;
  newsContent: any;
  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.news$ = this.getNews();
    this.news$.subscribe(val => this.newsContent = val);
    console.log(this.newsContent)
  }

  getNews() {
    return this.http
      .get("/assets/news.xml", { responseType: "text" })
      .pipe(
        switchMap(async xml => await this.parseXmlToJson(xml))
      );
  }

  async parseXmlToJson(xml) {
    // With parser
    /* const parser = new xml2js.Parser({ explicitArray: false });
    parser
      .parseStringPromise(xml)
      .then(function(result) {
        console.log(result);
        console.log("Done");
      })
      .catch(function(err) {
        // Failed
      }); */

    // Without parser
    return await xml2js
      .parseStringPromise(xml, { explicitArray: false })
      .then(response => response.Content.News);
  }

}
