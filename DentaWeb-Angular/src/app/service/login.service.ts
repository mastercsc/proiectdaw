import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  login(email:string, password:string) : Observable<any> {
    return this.http.post(`http://localhost:8282/dentaWeb_api/login.php`, { email, password });
}
}
