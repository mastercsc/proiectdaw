import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ContactService {

  contactUrl='http://127.0.0.1:5000/';
  constructor(private http: HttpClient) { }

  sendEmail(message:any) : Observable<any> {

    const body = { firstName: message.firstName,
                  lastName: message.lastName,
                  message: message.message,
                  phone: message.phone };
    console.log('body:', body);
    return this.http.post<any>(this.contactUrl, JSON.stringify(body));
  }
}