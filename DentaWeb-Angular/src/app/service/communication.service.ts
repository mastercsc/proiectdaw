import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommunicationService { 

public isLogin : boolean;

constructor() { }

    logout() {
        this.isLogin = false;
    }

    login() {
        this.isLogin = true;
    }
}