import { HttpClient } from '@angular/common/http';
import { Component, Injectable, OnInit } from '@angular/core';
import { User } from '../models/user';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`users`);
    }

    register(user: User) {
        console.log('user ', user);
        return this.http.post(`http://localhost:8282/dentaWeb_api/register.php`, user);
    }

    delete(id: number) {
        return this.http.delete(`/users/${id}`);
    }
}
