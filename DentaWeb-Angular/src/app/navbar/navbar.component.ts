import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CommunicationService } from '../service/communication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(private translate: TranslateService,
    private communication: CommunicationService) {
    translate.setDefaultLang('ro');
}
  ngOnInit(): void {
  }

  useLanguage(language: string): void {
    console.log(language);
    this.translate.use(language);
  }

  getLogin(){
    return this.communication.isLogin;
  }

  logout(){
    console.log("clear");
    sessionStorage.removeItem('username');
    sessionStorage.removeItem('password');
    this.communication.logout();
  }
}
