import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { LoginService } from '../service/login.service';
import { CommunicationService } from '../service/communication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private loginService: LoginService,
    private communication: CommunicationService
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loginService.login(this.f.username.value, this.f.password.value)
      .subscribe(
        data => {
          console.log('data: ', data);

          sessionStorage.setItem('username', this.f.username.value);
          sessionStorage.setItem('password', this.f.password.value);
          this.communication.login();
          if(data){
            if(data.rol == 'medic'){
              this.router.navigate(['/chart']);
            }
            else {
              this.router.navigate(['/home']);
            }
          }
          else {
            this.router.navigate(['/home']);
          }
        },
        error => {
          this.loading = false;
        });
  }
}