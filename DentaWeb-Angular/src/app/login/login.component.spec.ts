import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { Observable, of } from 'rxjs';
import { LoginComponent } from './login.component';
import { LoginService } from '../service/login.service';
import { Router } from '@angular/router';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let h1: HTMLElement;
  let mockLoginService;
  let router: Router;
  let vizitator: { "rol": null };


  beforeEach(async () => {
    // Create a fake LoginService object with a `login()` spy
    mockLoginService = jasmine.createSpyObj('LoginService', ['login']);
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes([]), 
        HttpClientTestingModule
      ],
      declarations: [ LoginComponent ],
      providers: [ { provide: LoginService, useValue: mockLoginService }]
                   //{ provide: Router, useValue: routerSpy }]
    }).compileComponents();
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    h1 = fixture.nativeElement.querySelector('h2');
    fixture.detectChanges();
    router = TestBed.get(Router);
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should have correct title', () => {
    expect(h1.textContent).toContain('Login');
  });

  it('form should be invalid if username is missing', async () => {
    component.loginForm.controls['username'].setValue('');
    component.loginForm.controls['password'].setValue('test');
    expect(component.loginForm.valid).toBeFalsy();
  });

  it('form should be invalid if password is missing', async () => {
    component.loginForm.controls['username'].setValue('test');
    component.loginForm.controls['password'].setValue('');
    expect(component.loginForm.valid).toBeFalsy();
  });

  it('form should be valid', async () => {
    component.loginForm.controls['username'].setValue('test');
    component.loginForm.controls['password'].setValue('test');
    expect(component.loginForm.valid).toBeTruthy();
  });
// fsdjkf
  it('should login as medic', async () => {
    //var medic: "medic";
    mockLoginService.login.and.returnValue(of("medic"));
    const navigateSpy = spyOn(router, 'navigate');
    component.loginForm.controls['username'].setValue('test');
    component.loginForm.controls['password'].setValue('test');
    component.onSubmit();
    expect (navigateSpy).toHaveBeenCalledWith(['/chart']);
  });

  it('should login as visitor', async () => {
    var visitor: {'rol': null};
    mockLoginService.login.and.returnValue(of(visitor));
    const navigateSpy = spyOn(router, 'navigate');
    component.loginForm.controls['username'].setValue('test');
    component.loginForm.controls['password'].setValue('test');
    component.onSubmit();
    expect (navigateSpy).toHaveBeenCalledWith(['/home']);
  });
});
