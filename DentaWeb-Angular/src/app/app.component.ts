import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { CommunicationService } from './service/communication.service';
import { LoginService } from './service/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private translate: TranslateService,
    private loginService: LoginService,
    private communication: CommunicationService) {
      translate.setDefaultLang('en');
      if(!communication.isLogin){
        var user = sessionStorage.getItem('username');
        var pass = sessionStorage.getItem('password');
        if(user && pass){
          this.loginService.login(user, pass)
          .subscribe(
            data => {
              this.communication.login();
            },
            error => {
              //this.alertService.error(error);
            });
        }
      }
  }
}
